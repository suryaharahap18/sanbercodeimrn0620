//TUGAS STRING

//Soal No.1(Membuat kalimat)
var word = 'JavaScript'; 
var second = 'is'; 
var third = 'awesome'; 
var fourth = 'and'; 
var fifth = 'I'; 
var sixth = 'love'; 
var seventh = 'it!';

var kalimat = word +' '+ second+' ' + third+' ' + fourth+' ' + fifth+' ' + sixth+' ' + seventh;
console.log(kalimat)
console.log(' ')

/*Output NO.1:
JavaScript is awesome and I love it! */

//Soal No.2(Mengurai kalimat - Akses karakter dalam string)
var sentance ="I am going to be React Native Developer";

var firstword = sentance[0];
var secondword = sentance[2] + sentance[3];
var thirdword = sentance[5] + sentance[6]+ sentance[7]+ sentance[8]+ sentance[9];
var fourthword= sentance[11]+sentance[12];
var fifthword= sentance[14]+sentance[15];
var sixthword= sentance[17]+sentance[18]+sentance[19]+sentance[20]+sentance[21];
var seventword= sentance[22]+sentance[23]+sentance[24]+sentance[25]+sentance[26]+sentance[27];
var eightword= sentance[29]+sentance[30]+sentance[31]+sentance[32]+sentance[33]+sentance[34]+sentance[35]+sentance[36]+sentance[37]+sentance[38];

console.log('First Word :'+firstword);
console.log('Second Word :'+secondword);
console.log('Third Second :'+thirdword);
console.log('Fourth Word :'+fourthword);
console.log('Fifth Word :'+fifthword);
console.log('Six Word :'+sixthword);
console.log('Seven Word :'+seventword);
console.log('Eight Word :'+eightword);
console.log(' ')

/*Output NO.2:
First word: I 
Second word: am 
Third word: going 
Fourth word: to 
Fifth word: be 
Sixth word: React 
Seventh word: Native 
Eighth word: Developer */


//Soal No.3 Mengurai kalimat(substring)
var sentance2 ='wow JavaScript is so cool';

var firstword2 = sentance2.substring(0,3);
var secondword2 = sentance2.substring(4,14);
var thirdword2 = sentance2.substring(15,17);
var fourthword2 = sentance2.substring(18,20);
var fifthword2 = sentance2.substring(21,25);

console.log('First Word : '+firstword2);
console.log('Second Word : '+secondword2);
console.log('Third Word : '+thirdword2);
console.log('Fourth Word : '+fourthword2);
console.log('Fifth Word : '+fifthword2);
console.log(' ')

/* Output No.3 
First Word: wow 
Second Word: JavaScript 
Third Word: is 
Fourth Word: so 
Fifth Word: cool 
*/

//Soal No.4 Mengurai kalimat dan menentukan panjang String
var sentance3 ='wow JavaScript is so cool';

var firstword3 = sentance2.substring(0,3);
var secondword3 = sentance2.substring(4,14);
var thirdword3 = sentance2.substring(15,17);
var fourthword3 = sentance2.substring(18,20);
var fifthword3 = sentance2.substring(21,25);

var firstwordlength =  firstword3.length
var secondwordlength = secondword3.length
var thirdwordlength = thirdword3.length
var fourthwordlength = fourthword3.length
var fifthwordlength = fifthword3.length 

console.log('First Word : '+firstword3 +', With length :'+firstwordlength);
console.log('Second Word : '+secondword3 +', With length :'+secondwordlength);
console.log('Third Word : '+thirdword3 +', With length :'+thirdwordlength);
console.log('Fourth Word : '+fourthword3 +', With length :'+fourthwordlength);
console.log('Fifth Word : '+fifthword3 +', With length :'+fifthwordlength);
